# Väre Data (public repository)

This repository contains code snippets and other stuff we might need.

## SmartCampus requests and data

The general request for all data points we need is contained in the file:
Request-SelectedDataPoints-Wed-Thu-Mittarilukema.xml
This request contains the dates, which should be changed.

In the directory **Data-Wed-Thu** there is some test data for the specific data points we want.

